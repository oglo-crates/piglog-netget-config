use netget::ProgressBarConfig;
use colored::Colorize;

pub fn piglog_netget_config() -> ProgressBarConfig {
    return ProgressBarConfig {
        message: format!("{l}{}{r} {}", "Download".bright_cyan().bold(), "${filename}", l = "[".bright_black().bold(), r = "] :".bright_black().bold()),
        bar: String::from("{msg} [{wide_bar:.cyan/blue}] [{elapsed_precise:.blue}] {bytes:.cyan}/{total_bytes:.cyan} ({bytes_per_sec:.green}, {eta:.magenta})"),
        chars: String::from("=> "),
    };
}
